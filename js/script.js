
function confirmStart() {
    if (confirm("Вы уверы что хотите выполнить это действие?")) {
        return true;
    } else {
        return false;
    }
}

function addSelect(domEl,nameSelect){
    let dirSelAuthor = document.getElementById(domEl);
    let strSelect = `<select name="${nameSelect}" class="input-add" id="selectAuthor">
                            <option value="null">Пусто</option>
                     </select>`;

    dirSelAuthor.insertAdjacentHTML('beforeend' , strSelect)

    let domSelectEl = document.querySelectorAll('.input-add');
    const el = domSelectEl[domSelectEl.length - 1]

    for (let key in arrAuthor){
        let strOption = `<option value="${arrAuthor[key]['id_author']}"> ${arrAuthor[key]['fullName']}</option>`;
        el.insertAdjacentHTML('beforeend' , strOption);
    }
}


function showAuthors(){
    let checkbox = document.querySelector('.container-checkbox')

    if(checkbox.classList.contains("active")){
        checkbox.classList.remove("active");
    }
    else{
        checkbox.classList.add("active")
    }
}
