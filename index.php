<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css" integrity="sha512-NmLkDIU1C/C88wi324HBc+S2kLhi08PN5GDeUVVVC/BVt/9Izdsc9SVeVfA1UZbY3sHUlDSyRXhCzHfr6hmPPw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href='/css/style.css'>
    <title>first_db</title>
</head>

<?php

$link = mysqli_connect('localhost', 'root', 'root', 'sql_less'); // Соединяемся с базой

// Ругаемся, если соединение установить не удалось
if (!$link) {
    echo 'Не могу соединиться с БД. Код ошибки: ' . mysqli_connect_errno() . ', ошибка: ' . mysqli_connect_error();
    exit;
}
session_start();
define('LIMIT',4);// лимит книг на странице

function getValue($name){
    return $_SESSION[$name];
}
// функции для того что бы при изменении хранилища данных не нужно было переписавать код
function setValue($name,$value,$flag = false){
    if ($flag){
        $_SESSION[$name][] = $value; // для многомерного сохранения
    }else{
        $_SESSION[$name] = $value;
    }
}

// количество страниц для пагинатора
function countPages($link){
    if (!empty($_SESSION["authorPost"])){
        $idAuthor = $_SESSION["authorPost"];
        $strIdAuthor = implode(',',$idAuthor);
        $getInfBook = mysqli_query($link,"SELECT COUNT(DISTINCT id_book) FROM `book_author` WHERE book_author.id_author in ($strIdAuthor)");
    }else{
        $getInfBook = mysqli_query($link,"SELECT COUNT(DISTINCT id_book) FROM book ");

    }

    foreach ($getInfBook as $item) {
        $countBook = $item['COUNT(DISTINCT id_book)'];
    }

    $result = $countBook/LIMIT; // количество книг на лимит на странице
    $result = ceil($result); // округляет в болшую сторону
    for($i=0; $i < $result; $i++){ // создает по количеству масив для foreach
        $arrResult[] = $i+1;
    }
    return $arrResult;
}

// для офсет / вычисляет по странице с какой книги выводить
function createNumOffset($page){
    $offSet = ($page-1)*LIMIT;
    return $offSet;
}

// для первой страницы(текущая страница)
function currentPage(){
    if (isset($_GET['btnAddBook']) || isset($_GET['editBook'])){
        $numberPage = false;
    }elseif (empty($_GET['page'])){
        $numberPage = 1;
    } else{
        $numberPage = $_GET['page'];
    }
    return $numberPage;
}

// обьединение авторов с книгой , функция для рендера на страницу
function generateBooks($link){
    foreach (getInfBook($link) as $keyBook => $item) {
        foreach (getAuthors($link) as $key => $value) {
            if ($keyBook == $key){
                $mergeArr[] = array_merge($item,$value);
            }
        }
    }
    return $mergeArr;
}


// сохр/удаление ид автора при фильтре
if (isset($_POST['filter-book'])){
    $_SESSION["authorPost"] = $_POST['author'];
    header("location: /");
}
if (isset($_POST['delete-filter']) || $_POST['author'][0] == 'null'){
    deleteFilter();
}
function deleteFilter(){
    unset($_SESSION['authorPost']);
}
// -----



//тянет книги
function getInfBook($link){
    //с фильтром
    if (!empty($_SESSION["authorPost"])){
        $idAuthor = $_SESSION["authorPost"];
        $strIdAuthor = implode(',',$idAuthor);
        $numOffset = createNumOffset(currentPage());
        $getInfBook = mysqli_query($link,"SELECT book.*,image.img FROM book LEFT JOIN image on book.isbn = image.isbn WHERE book.id_book in (SELECT DISTINCT id_book FROM `book_author` WHERE book_author.id_author in ($strIdAuthor)) LIMIT ".LIMIT." OFFSET ".$numOffset);

    }else{
        $numOffset = createNumOffset(currentPage());
        $getInfBook = mysqli_query($link,"SELECT book.*,image.img FROM book LEFT JOIN image on book.isbn = image.isbn LIMIT ".LIMIT." OFFSET ".$numOffset);
    }
    foreach ($getInfBook as $item) {
        $arrAllInfBook[$item['id_book']] = $item;

    }

    return $arrAllInfBook;
}

// тянет авторов
function getAuthors($link){
    // тяную ид книг
    $idBook = array_keys(getInfBook($link) );
    //формирую в 1 строку все ид для запроса
    $strIdBook = implode(',',$idBook);
    //1 запросом тяну нужние книги с ид авторов
    $book_author = mysqli_query($link,"SELECT id_author , id_book FROM book_author WHERE book_author.id_book in ($strIdBook)");
    foreach ($book_author as $item) {
        $idAuthor[$item['id_book']][]= $item['id_author'];
    }
    // по ид тяну имена
    foreach ($idAuthor as $key => $item) {
        foreach ($item as $item2) {
            $getFullName = mysqli_query($link,"SELECT fullName FROM author WHERE author.id_author ='$item2'");
            foreach ($getFullName as $item3){
                $fullName[$key][]["fullName"] = $item3['fullName'];
            }
        }
    }
    return $fullName;
}


if (!empty($_POST['delBook'])){
    delBook($link);
}
function delBook($link){
    $idBtn = $_POST['delBook'];
    $isbn = mysqli_query($link,"SELECT isbn FROM book WHERE book.id_book = '$idBtn'");
    foreach ($isbn as $item) {
        $isbnStr = $item['isbn'];
}
    if ($getNameImg = mysqli_query($link,"SELECT img FROM image WHERE image.isbn = '$isbnStr'")){
        foreach ($getNameImg as $item) {
            $nameImg = $item['img'];
        }
    }
    foreach ($getNameImg as $item) {
        $nameImg = $item['img'];
    }

    mysqli_query($link,"DELETE FROM book_author WHERE book_author.id_book = '$idBtn'");
    mysqli_query($link,"DELETE FROM book WHERE book.id_book = '$idBtn'");

    unlink('img/'.$nameImg);
}



//для рендера авторов
function getAllAuthor($link){
    $getIdStrl = mysqli_query($link, "SELECT fullName,id_author FROM author");
    foreach ($getIdStrl as $item){
        $authorFromDB[] = $item;
    }
    return $authorFromDB;
}

function getNameAuthorFromDB($link){
    $idBook = $_GET['editBook'];
    $getIDAuthor = mysqli_query($link,"SELECT id_author FROM book_author WHERE book_author.id_book = '$idBook'");
    foreach ($getIDAuthor as $item) {
        $idAuthor[] = $item;
    }

    foreach ($idAuthor as $item){
        $id = $item['id_author'];
        $getName = mysqli_query($link,"SELECT fullName FROM author WHERE author.id_author = '$id'");
        foreach ($getName as $value) {
            $name[$id] = $value;
        }
    }
    return $name;
}


if (!empty($_POST['addBook']) || !empty($_POST['btnEditNameBook'])){
    $printError = checkNewBook();
}

// проверка книги
function checkNewBook(){
    $idAuthorPost = $_POST['author'];
    $idNewAuthorPost = $_POST['addAuthor'];
    $isbn = $_POST['isbn'];

    if (!is_numeric($isbn)){
        $err['isbn'] = 'В поле "isbn" можно вводить только числа';
    }
    if (empty($isbn)){
        $err['isbn'] = 'Поле isbn обязательно для ввода';
    }
    if (count($isbn) > 10){
        $err['isbn'] = 'Не более 10 цифр';
    }
    if (empty($_POST['nameBook'])){
        $err['nameBook'] = 'Поле: "Название Книги" обязательно для ввода';
    }
    if (count($idAuthorPost) == 1 ){
        foreach ($idAuthorPost as $item) {
            if ($item == "null"){
                $err['author'] = 'Поле: "Автор" обязательно для ввода';
            }
        }
    }
    $checkRepeat = array_count_values($idAuthorPost); // если есть одинаковый автор -> покажет количество одинаковых(если больше 1 - ошибка)
    foreach ($checkRepeat as $key => $item){
        if ($key == 'null'){
            unset($checkRepeat[$key]); // избавляюсь от сценария если есть много "null"
            $cleanArr = $checkRepeat;
        }
    }
    foreach ($cleanArr as $item) {
        if ($item > 1 ){
            $err['author'] = 'Выбрано несколько одинаковых авторов';
        }
    }
    if (!empty($idNewAuthorPost)){
        foreach ($idAuthorPost as $item){
            foreach ($idNewAuthorPost as $item2) {
                if ($item == $item2){
                    $alert = 'Выбрано несколько одинаковых авторов';
                }
            }
        }
    }

    if (!empty($alert)){
        $err['author'] = $alert;
    }

    if (empty($err)){
        return 1;
    }else{
        return $err;
    }
}
// int
if ($printError == 1 && !empty($_POST['addBook'])){
    addNewBook($link);
}
function addNewBook($link){
    $isbn = $_POST['isbn'];
    $nameBook = $_POST['nameBook'];

    //массив авторов
    foreach ($_POST['author'] as $item) {
        $arrIdAuthor[] = $item;
    }
    $arrIdAuthor_unique = array_unique($arrIdAuthor); // исключает вариант повтора

//добавляет книгу
    mysqli_query($link,"INSERT INTO book( isbn , nameBook ) VALUES ('$isbn','$nameBook')");
// добавляет картинку
    $data = $_FILES ['file'];
    $tmp = $data ['tmp_name'];
    if (file_exists($tmp)){
        $nameImgBook = addImgDir();
        mysqli_query($link,"INSERT INTO image( isbn , img ) VALUES ('$isbn','$nameImgBook')");
    }
//id книги
    $getIdBook = mysqli_query($link,"SELECT id_book FROM book WHERE book.nameBook = '$nameBook'");
    foreach ($getIdBook as $item) {
        $idBookFromDB = $item['id_book'];
    }
//запись в промежуточную таблицу
    foreach ($arrIdAuthor_unique as $item) {
        if ($item !== 'null'){
            mysqli_query($link,"INSERT INTO `book_author` (`id_author`, `id_book`) VALUES ( '$item', '$idBookFromDB')");
        }
    }
//поверка есть ли книга в базе если да переводит на главную
    checkDB_AfterAdd($link);
}

//поверка есть ли книга в базе после добавления
function checkDB_AfterAdd($link){
    $isbn = $_POST['isbn'];
    $test = mysqli_query($link, "SELECT isbn FROM book WHERE book.isbn in('$isbn')");
    foreach ($test as $item) {
        $newIsbn = $item['isbn'];
    }
    if ($isbn == $newIsbn) {
        $_SESSION['errTitle'] = true;
        header("Location: http://localhost:8888/?page=".(array_key_last(countPages($link))+1)); //переведет на последнюю страницу
    }else{
        $_SESSION['errTitle'] = false;
    }
}


function addImgDir(){
    $imgDir = "img"; // каталог для хранения изображений
    if (!file_exists($imgDir)){
        mkdir($imgDir, 0777); // создаем каталог, если его еще нет
    }
    $data = $_FILES ['file'];
    $tmp = $data ['tmp_name'];

    if (file_exists($tmp)) {// Проверяем, принят ли файл,
        $info = getimagesize($_FILES ['file'] ['tmp_name']); //Функция вернет размер изображения, тип файла, height, width, а также тип содержимого HTTP
        // Проверяем, является ли файл изображением,
        if (preg_match('{image/(.*)}is', $info['mime'], $p)) {
            // Имя берем равным текущему времени в секундах, а
            // расширение — как часть MIME-типа после "image/".
            $name = time() . "." . $p [1];

            // Добавляем файл в каталог с фотографиями.
            move_uploaded_file($tmp, "$imgDir/".$name);
            return $name;
        }
    }
}


if ($printError == 1 && isset($_POST['btnEditNameBook'])){
    editBook($link);
}
// для того что бы после редактирования вернутся на туже страницу
if (!empty($_GET['editBook'])){
    $_SESSION['page'] = $_POST['page'];
}

// редактировать книгу сохряняет в сесию добавил или нет
function editBook($link){

    $editIsbn = $_POST['isbn'];
    $editNameBook = $_POST['nameBook'];
    $idAuthors = $_POST['author'];
    $idBook = $_GET['editBook'];
    $idNewAuthor = $_POST['addAuthor'];


    if (mysqli_query($link,"UPDATE `book` SET `isbn` = '$editIsbn', `nameBook` = '$editNameBook' WHERE `book`.`id_book` = '$idBook'")){
        // редактирование автора
        foreach ($idAuthors as $idAuthor => $value) {
            if ($value !== 'null'){
                mysqli_query($link,"UPDATE book_author SET id_author = '$value' WHERE book_author.id_author = '$idAuthor'");
            }else{
                // чистка автора(опционально)
                $idArr = mysqli_query($link,"SELECT id FROM book_author WHERE book_author.id_author = '$idAuthor' and book_author.id_book = '$idBook'");
                foreach ($idArr as $item){
                    $id = $item['id'];
                }
                mysqli_query($link,"DELETE FROM book_author WHERE book_author.id = '$id'");
            }
        }
        //добавляет нового автора
        if ($idNewAuthor){
            foreach ($idNewAuthor as $item) {
                if ($item !== 'null'){
                    mysqli_query($link,"INSERT INTO book_author (id_author,id_book) VALUES ('$item','$idBook')");
                }
            }
        }
        //обновление изображения
        $data = $_FILES ['file'];
        $tmp = $data ['tmp_name'];
        if (file_exists($tmp)){// Проверяем, принят ли файл,
            $getNameImg = mysqli_query($link,"SELECT img FROM image WHERE image.isbn = '$editIsbn'");
            foreach ($getNameImg as $item) {
                $nameImgOld = $item['img'];
            }
            $nameImg = addImgDir();
            $updateImg = mysqli_query($link, "UPDATE `image` SET `img` = '$nameImg' WHERE `image`.`isbn` = '$editIsbn'");

            // если нету картинки
            if (empty($nameImgOld)){
                mysqli_query($link,"INSERT INTO image (isbn,img) VALUES ('$editIsbn','$nameImg')");
            }
        }
        //чистка старой картинки
        if ($updateImg){
            unlink('img/'.$nameImgOld);
        }
        $_SESSION['errTitle'] = true;
        header("Location: http://localhost:8888/?page=".$_SESSION['page']);
    }
}


//для страницы редактирования(view_Edit)
function getBookByID($link){
    $idBook = $_GET['editBook'];

    $getIdStrl = mysqli_query($link, "SELECT * FROM book WHERE id_book in('$idBook')");
    foreach ($getIdStrl as $value){
        $arrBook = $value;
        $bookIsbn = $value['isbn'];
    }

    $getImg = mysqli_query($link, "SELECT img FROM image WHERE isbn in('$bookIsbn')");
    foreach ($getImg as $item){
        $arrImg = $item;
    }
    if(!empty($arrImg)){
        $mergeBookImg[] = array_merge($arrBook,$arrImg);
    }
    $arrBookT[] = $arrBook;

    if (empty($_POST['btnEditNameBook'])){
        if (empty($mergeBookImg)){
            return $arrBookT;
        }else{
            return $mergeBookImg;
        }
    }else{
        $postPrint[] = $_POST;
        return $postPrint;
    }
}

// шаблон сообщения
function templateAlert($flagUpd){
    if ($flagUpd){
        $errTitle = "Успешно изменено";
    }else{
        $errTitle =  'Что то пошло не так , попробуйде позже';
    }
    return $errTitle;
}

if (isset($_POST['closebtn']) ){
    unset($_SESSION['errTitle']);
}

?>



<body>
<div class="main">
<header class="header"></header>

    <?php
//     фильто по автору
    if (currentPage()){
        include 'view/selectAuthor_view.php';
    }
    // ошибки
    if (!empty($printError) && $printError !== 1){
        include 'view/err_view.php';
    }
        // оповещение
    if (isset($_SESSION['errTitle'])){
        include 'view/alert_view.php';
    }

    // страница создания книги
    if (isset($_GET["btnAddBook"])) {
        include 'view/addBook_view.php';
    }

    //кнопка добавить книгу
    if (empty($_GET['btnAddBook'])){
        include 'view/btnAddBook_view.php';
    }

    //все книги
    if (currentPage()){
        include "view/library_view.php";
    }

//    if (!empty($_GET['page'])){
    if (currentPage()){
        include "view/paginator_view.php";
    }

    //страница редактирования книги
    if (isset($_GET["editBook"])){
        include 'view/editBook_view.php';
    }


//    include 'js/addselect.php';
    ?>



<footer class="footer"></footer>
</div>
<script>
    let arrAuthor = <?php echo json_encode(getAllAuthor($link)) ?>;
</script>
<script src="js/script.js"></script>
</body>
</html>
