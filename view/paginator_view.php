
<div class="paginator">
    <?php foreach (countPages($link) as $item):?>
        <a class="<?php if ($_GET['page'] == $item || currentPage() == $item)  echo 'current'?>" href="?page=<?php echo $item ?>"><?php echo $item ?></a>
    <?php endforeach;?>
</div>