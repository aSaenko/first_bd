
<div class="container-select-filter">
    <p class="filter-title">Фильтровать по автору</p>
    <form action="" method="post" class="form-filter">

        <div class="dropdown-checkbox">
            <svg onclick="showAuthors()" class="dropdown-svg" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" aria-hidden="true"><path d="M6.984 9.984h10.031L11.999 15z"/></svg>
            <ul class="container-checkbox">
                <?php foreach (getAllAuthor($link) as $value):?>
                    <li class="checkbox-author"><input class="checkbox-author-input" <?php if (in_array($value['id_author'],$_SESSION['authorPost'])) echo 'checked'?>  type="checkbox" name="author[]" value="<?php echo $value['id_author'] ?>"><?php echo $value['fullName'] ?></li>
                <?php endforeach;?>
            </ul>
        </div>

        <div class="container-btn-filter">
            <input name="filter-book" type="submit" class="btn-add-book" value="Фильтровать">
            <input name="delete-filter" type="submit" class="btn-delete-filter" value="Delete" onclick="return confirmStart()">
        </div>
    </form>
</div>
