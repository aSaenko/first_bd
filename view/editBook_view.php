
<div class="wrapper-btn-header">
    <a href="/" class="btn-header">На главную</a>
</div>

<div class="wrapper-main-block">
    <div class="edit-book">
        <form action="" method="post" class="container-inputs" enctype="multipart/form-data">
            <h1 class="title-edit" >Редактировать книгу :</h1>
            <?php foreach (getBookByID($link) as $item):?>
                <p>Номер книги(isbn)</p>
                <input name="isbn" type="text" class="input-add isbn <?php if (!empty($printError['isbn'])) echo 'active' ?>" value="<?= $item['isbn'] ?>">
                <p>Название Книги</p>
                <input name="nameBook" type="text" class="input-add nameBook <?php if (!empty($printError['nameBook'])) echo 'active' ?>" value="<?= $item['nameBook'] ?>">

                <div class="wrapper-select" id="select-author">
                    <p>Автор</p>

                    <?php if (!empty($_POST['author'])) :?>
                        <?php foreach ($_POST['author'] as $key => $value2):?>
                            <select name="author[<?php echo $key ?>]" class="input-add" id="selectAuthor">
                                <option value="null">Пусто</option>
                                <?php foreach (getAllAuthor($link) as $value):?>
                                    <option <?php if ($key == $value['id_author']) echo 'selected'?> value="<?php echo $value['id_author'] ?>"> <?php echo $value['fullName']  ?> </option>
                                <?php endforeach;?>
                            </select>
                        <?php endforeach;?>

                        <?php foreach ($_POST['addAuthor'] as $key => $value2):?>
                            <select name="addAuthor[<?php echo $key ?>]" class="input-add" id="selectAuthor">
                                <option value="null">Пусто</option>
                                <?php foreach (getAllAuthor($link) as $value):?>
                                    <option <?php if ($value2 == $value['id_author']) echo 'selected'?> value="<?php echo $value['id_author'] ?>"> <?php echo $value['fullName']  ?> </option>
                                <?php endforeach;?>
                            </select>
                        <?php endforeach;?>
                    <?php endif;?>


                    <?php if (empty($_POST['author'])) :?>
                        <?php foreach (getNameAuthorFromDB($link) as $key => $value2):?>
                            <select name="author[<?php echo $key ?>]" class="input-add" id="selectAuthor">
                                <option value="null">Пусто</option>
                                <?php foreach (getAllAuthor($link) as $value):?>
                                    <option <?php if ($key == $value['id_author']) echo 'selected'?> value="<?php echo $value['id_author'] ?>"> <?php echo $value['fullName']  ?> </option>
                                <?php endforeach;?>
                            </select>
                        <?php endforeach;?>
                    <?php endif;?>

                </div>
                <div class="addAuthor" id="addAuthor" onclick="addSelect('select-author','addAuthor[]')">Добавить автора</div>
                <hr>

                <div class="wrapper-img-add-book">
                    <section class="section-img">
                        <p class="header-img">Изменить изображение</p>
                        <input type="file" name="file" value="">
                    </section>
                    <input name="btnEditNameBook" type="submit" class="btn-add-book" onclick="return confirmStart()">
                </div>

            <?php endforeach;?>
        </form>
    </div>

</div>




