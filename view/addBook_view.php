
<div class="wrapper-btn-header">
    <a href="/" class="btn-header">На главную</a>
</div>

<div class="wrapper-main-block">

    <form action="" method="post" class="container-inputs" enctype="multipart/form-data">
        <p>Номер книги(isbn)</p>
        <input name="isbn" type="text" class="input-add isbn <?php if (!empty($printError['isbn'])) echo 'active' ?>" value="<?php echo $_POST['isbn'] ?>">
        <p>Название Книги</p>
        <input name="nameBook" type="text" class="input-add nameBook<?php if (!empty($printError['nameBook'])) echo 'active' ?>" value="<?php echo $_POST['nameBook'] ?>">
        <p>Автор</p>
        <div id="wrapper-select-addBook">

            <?php if (empty($_POST['author'])) :?>
                <select name="author[]" id="" class="input-add">
                    <option value="null">Пусто</option>
                    <?php foreach (getAllAuthor($link) as $value):?>
                        <option <?php if ($item == $value['id_author']) echo 'selected'?> value="<?php echo $value['id_author'] ?>"> <?php echo $value['fullName'] ?> </option>
                    <?php endforeach;?>
                </select>
            <?php endif;?>

            <?php foreach ($_POST['author'] as $item):?>
                <select name="author[]" id="" class="input-add">
                    <option value="null">Пусто</option>
                    <?php foreach (getAllAuthor($link) as $value):?>
                        <option <?php if ($item == $value['id_author']) echo 'selected'?> value="<?php echo $value['id_author'] ?>"> <?php echo $value['fullName'] ?> </option>
                    <?php endforeach;?>
                </select>
            <?php endforeach;?>

        </div>

        <div class="addAuthor" id="addAuthor" onclick="addSelect('wrapper-select-addBook','author[]')">Добавить автора</div>
        <hr>

        <div class="wrapper-img-add-book">
            <section class="section-img">
                <p class="header-img">Добавить изображение:</p>
                <input class="btn-add-img" id="input__file" type="file" name="file" value="">
            </section>
            <input name="addBook" type="submit" class="btn-add-book" >
        </div>

    </form>
</div>



