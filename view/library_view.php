
<div class="library">

    <?php foreach (generateBooks($link) as $value):?>

        <div class="book">
            <img class="img-library" src="/img/<?php echo $value['img'] ?>" alt="">

            <div class="title-container">
                <h1 class="name-book"><?php echo $value['nameBook'] ?></h1>
                <form action="" method="post">
                    <?php foreach ($value as $key => $value1):?>
                        <?php foreach ($value1 as $name) :?>

                            <input name="author" type="submit" class="author" value="<?php echo $name?>">

                        <?php  endforeach;?>
                    <?php  endforeach;?>
                </form>

                <?php if($_POST['author'] == $value['fullName']):?>

                    <p class="date-of-birth"><?php echo $value['birsdayDate'] ?></p>
                    <p class="country"><?php echo $value['country'] ?></p>

                <?php endif;?>

            </div>
            <form action="<?php echo '?editBook='.$value["id_book"] ?>" method="post">
                <input type="hidden" name="page" value="<?php echo $_GET['page']?>">
                <input class="btn-edit-book" type="submit" value="Редактировать">
            </form>

            <form action="" method="post">
                <button name="delBook" class="btn-delete-book" type="submit" value="<?= $value["id_book"] ?>" onclick="return confirmStart()">x</button>
            </form>

        </div>

    <?php  endforeach;?>

</div>